#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
.. versionadded:: 0.5.0
"""

import copy
import os
from typing import Dict, Iterable, Sequence, Tuple

from .field import Field, TypedField, data_decorator

__all__ = ("IndexableField", "NestedField", "ArrayField", "DictField")


@data_decorator
class IndexableField(Field):
    """Represents a mutable and indexable data type."""

    # Python primitives converters
    @classmethod
    def from_primitives(cls,
                        obj: Iterable[object],
                        fieldtypes: Dict[type, type] = None,
                        root_type: type = None,
                        description: str = "",
                        readonly: bool = False,
                        **attributes: object) -> Field:
        """
        Create an :class:`~IndexableField` instance
        from Python primitives.

        .. versionadded:: 0.4.0
        """

        def _type_checker(fieldkey, fieldval) -> Field:  # type: ignore
            """Check field type and encapsulate key and val into specific field instance."""
            try:
                # try direct association
                final_fieldtype = fieldtypes[type(fieldval)]

            except KeyError:
                # fallback to for loop
                for datatype, fieldtype in fieldtypes.items():
                    if isinstance(fieldval, datatype):
                        final_fieldtype = fieldtype
                        break
                else:
                    raise NotImplementedError(
                        "Unimplemented data type {} of {}".format(
                            type(fieldval), fieldval))
            if issubclass(final_fieldtype, IndexableField):
                fieldinst = cls.from_primitives(
                    fieldval,
                    root_type=final_fieldtype,
                    fieldtypes=fieldtypes,
                    description=description,
                    readonly=readonly,
                    **attributes)
                fieldinst.name = fieldkey
            else:
                fieldinst = final_fieldtype(
                    name=fieldkey,
                    data=fieldval,
                    description=description,
                    readonly=readonly,
                    **attributes)
            return fieldinst

        if fieldtypes is None:
            fieldtypes = cls._default_datatypes
        if root_type is None:
            root_type = cls

        if isinstance(obj, dict):
            rootfield = root_type(
                name=None,
                data={
                    key: _type_checker(key, value)
                    for key, value in obj.items()
                },
                description=description,
                readonly=readonly,
                **attributes)
        else:
            rootfield = root_type(
                name=None,
                data=[_type_checker(None, value) for value in obj],
                description=description,
                readonly=readonly,
                **attributes)
        return rootfield

    def to_primitives(self, use_name: bool = False) -> object:
        """
        Convert the field tree into Python primitives.

        :param bool use_name:
            If True, try to use use the field name as containter key,
            This works good for dict-like object, but fails on arrays,
            so a progressive int will be used instead.

        .. warning::
            This method does not do any type casting,
            it only extracts name and data from fields.

        .. versionadded:: 0.4.0
        """

        def _recursive_extractor(field: Field) -> object:
            datacopy = copy.copy(field.data)

            if isinstance(field, IndexableField):
                for key, val in field.iteritems():
                    oldkey = None
                    if use_name and val.name != key:
                        oldkey = key
                        key = val.name

                    extracted = _recursive_extractor(val)

                    try:
                        datacopy[key] = extracted
                    except TypeError:
                        datacopy[oldkey] = extracted
                    else:
                        if oldkey is not None:
                            del datacopy[oldkey]

            return datacopy

        return _recursive_extractor(self)

    # reimplement data
    def getData(self) -> Sequence[Field]:  # pylint: disable=W0235
        return super().getData()

    def setData(self, value: Sequence[object]) -> None:
        """Check if the given data is iterable."""
        if hasattr(value, "__iter__") and hasattr(value, "__getitem__"):
            super().setData(value)
        else:
            raise TypeError("Given data typed {} is not iterable".format(
                type(value).__name__))

    # index operators
    def __getitem__(self, key: object) -> Field:
        """Implement :code:`obj[key]`"""
        return self.data.__getitem__(key)

    def __setitem__(self, key: object, val: object) -> None:
        """Implement :code:`obj[key] = val`"""
        if isinstance(val, Field):
            return self.data.__setitem__(key, val)
        raise TypeError(
            "Given data '{}' isn't an instance or Field or NoneType.".format(
                val))

    def __delitem__(self, key: object) -> None:
        """Implement :code:`del obj[key]`"""
        return self.data.__delitem__(key)

    # comparison operators
    def __contains__(self, other: Field) -> bool:
        """
        Check if the given field is contained (the 'in' operator)
        in the field data.

        :raises TypeError: If :obj:`~Field.data` does not support this operator.

        .. versionadded:: 0.2.0
        """
        return self._data in other.data

    # data iterations
    def __iter__(self) -> Iterable[Field]:
        """Iterate data."""
        return self.data.__iter__()

    def iteritems(self) -> Iterable[Tuple[object, Field]]:
        """
        Iterate data items.

        By default, this method yields pairs containing a count,
        which start from zero, and the yielded value, as enumerate does.

        Subclasses must implement this method if first key
        isn't zero or it isn't a number.

        .. versionadded:: 0.4.0
        """
        return enumerate(self)

    # data properties
    def __len__(self) -> int:
        """
        Return the length of data.

        :raises TypeError: If :obj:`~Field.data` does not support this operator.

        .. versionadded:: 0.2.0
        """
        return len(self._data)

    # child manipulation
    def add(self, field: Field) -> None:
        """
        Add a :class:`~Field` object.

        Subclasses must implement this method if adding
        requires a different manner to be done.

        :raises IndexError:
            If field name isn't a valid key.

        .. versionadded:: 0.4.0
        """
        self[field.name] = field

    def remove(self, field: Field) -> None:
        """
        Remove first occurrence of given :class:`~Field` object.
        By default, it removes the field by its name.

        Subclasses must implement this method if removing
        requires a different manner to be done.

        :raises KeyError:
            If given :class:`~Field` object is not found.

        .. versionadded:: 0.4.0
        """
        del self[field.name]

    def extend(self, other: "IndexableField") -> None:
        """
        Add the field children in the given :class:`IndexableField` object.

        .. versionadded:: 0.5.0
        """
        for field in other:
            self.add(field)

    def clear(self) -> None:
        """
        Remove all the field.

        .. versionadded:: 0.5.0
        """
        for key, field in self.iteritems():
            try:
                del self[key]
            except LookupError:
                self.remove(field)

    # utils
    def prettify(self,
                 indentation: int = 0,
                 indentation_str: str = " " * 4,
                 to_print: bool = False) -> str:

        def _ind(_str):
            return indentation_str * indentation + _str

        if to_print:
            print(
                _ind("Field details:"),
                _ind("type:        {}".format(type(self).__name__)),
                _ind("name:        {}".format(self.name)),
                _ind("description: {}".format(self.description)),
                _ind("readonly:    {}".format(self.readonly)),
                _ind("attributes:  {}".format(
                    os.linesep.join("{} => {}".format(key, val)
                                    for key, val in self.attributes.items()))),
                _ind("Data elements:"),
                sep=os.linesep)
        else:
            retstr = ""
            retstr += _ind("Field details:") + os.linesep
            retstr += _ind("type:        {}".format(
                type(self).__name__)) + os.linesep
            retstr += _ind("name:        {}".format(self.name)) + os.linesep
            retstr += _ind("description: {}".format(self.descr)) + os.linesep
            retstr += _ind("readonly:    {}".format(self.readonly)) + os.linesep
            retstr += _ind("attributes:{}{}".format(
                os.linesep,
                os.linesep.join(
                    "{} => {}".format(key, val)
                    for key, val in self.attributes.items()))) + os.linesep
            retstr += _ind("Data elements:") + os.linesep

        for field in self:
            fieldstr = field.prettify(
                indentation + 1,
                indentation_str=indentation_str,
                to_print=to_print)
            if to_print:
                print()
            else:
                retstr += fieldstr + os.linesep
        if not to_print:
            return retstr

    def count(self) -> int:
        """
        Recursively count the number of :class:`~Field` objects, including itself.

        If there is no children, 1 is returned.
        """

        counter = 1
        for child in self:
            if isinstance(child, IndexableField):
                counter += child.count()
            else:
                counter += 1

        return counter


class NestedField(IndexableField):
    """Represents a structured data that must contain instances of :class:`~Field` only."""

    def iteritems(self, i: int = 0) -> Iterable[Tuple[object, Field]]:
        for value in self._data:
            if isinstance(value, NestedField):
                yield from value.iteritems(i)
            elif isinstance(value, Field):
                yield i, value
            else:
                # It's very difficult to raise that exception
                raise TypeError("Found unvalid object typed {}".format(
                    type(value).__name__))
            i += 1

    def __iter__(self) -> Iterable[Field]:
        """Iterate over all children fields."""
        for _, value in self.iteritems():
            if isinstance(value, NestedField):
                yield from value.__iter__()
            elif isinstance(value, Field):
                yield value
            else:
                # It's very difficult to raise that exception
                raise TypeError("Found unvalid object typed {}".format(
                    type(value).__name__))


class ArrayField(IndexableField, metaclass=TypedField):
    """Represents a list-list field."""
    __TYPEOBJ__ = list

    def add(self, field: Field) -> None:
        """Add a :class:`Field` using :meth:`list.append`."""
        if self._data is None:
            self._data = self.__TYPEOBJ__()
        self.data.append(field)

    def append(self, *args, **kwargs):
        """
        An alias for :code:`field.data.append()`.

        .. versionadded:: 0.2.0
        """
        self.data.append(*args, **kwargs)

    def insert(self, *args, **kwargs):
        """
        An alias for :code:`field.data.insert()`.

        .. versionadded:: 0.2.0
        """
        self.data.insert(*args, **kwargs)

    def index(self, *args, **kwargs):
        """
        An alias for :code:`field.data.index()`.

        .. versionadded:: 0.2.0
        """
        self.data.index(*args, **kwargs)

    def extend(self, other: IndexableField) -> None:
        """
        An alias for :code:`field.data.extend()`.

        .. versionadded:: 0.2.0

        .. versionchanged:: 0.5.0
            The signature match the :meth:`IndexableField.extend` one.
        """
        self.data.extend(other)

    def remove(self, field: Field) -> None:
        try:
            self.data.remove(field)
        except (ValueError, KeyError) as ex:
            raise KeyError(*ex.args)

    def clear(self) -> None:
        self._data.clear()


class DictField(IndexableField, metaclass=TypedField):
    """Represents a dictionary-like field."""

    __TYPEOBJ__ = dict

    def add(self, field: Field):
        if self._data is None:
            self._data = self.__TYPEOBJ__()
        super().add(field)

    def items(self):
        """
        An alias for :code:`field.data.items()`.

        .. versionadded:: 0.2.0
        """
        return self.data.items()

    def keys(self):
        """
        An alias for :code:`field.data.keys()`.

        .. versionadded:: 0.2.0
        """
        return self.data.keys()

    def values(self):
        """
        An alias for :code:`field.data.values()`.

        .. versionadded:: 0.2.0
        """
        return self.data.values()

    def clear(self) -> None:
        self._data.clear()

    def extend(self, other: IndexableField) -> None:
        self._data.update(**other.iteritems())

    __iter__ = lambda self: iter(self.values())
    """
    Iterate over values to behave as specified in :meth:`IndexableField.__iter__`.

    .. versionadded:: 0.2.0
    """

    iteritems = items
