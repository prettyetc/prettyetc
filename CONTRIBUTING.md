# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
Discord server, email, or any other method with the owners of this repository before making a change.

Please note we have a code of conduct, please follow it in all your interactions with the project.

## Pull Request Process

Note: Gitlab calls it "Merge Request" instead of "Pull Request".

1. Ensure any install or build dependencies are removed before the end of the layer when doing a build.

2. Check pipelines status and ensure that all the jobs passed. Editing the .gitlab-ci.yml file without any valid reason is forbidden.

3. In the pull request, specify in details the changes made to the project or the interface, this includes changes to APIs, UI design, useful file locations or other.

4. You may merge the Pull Request if you are a Prettyetc mantainer, otherwise you may request a reviewer to merge it for you.

## Guidelines

### Code style

Code style is very important in the Prettyetc development, to get the maximum readability in code and standardization in APIs.

Currently, I use yapf with google style as formatter and pylint (its settings can be found in `useful_for_devs/pylintrc` file) as linter, but you can use your preferred formatter and linter. Anyway, remember to check pylint score in the pylint CI job. The minimum score depends on number of changes. Bigger changes may allow to reduce the minimum score.

Naming conventions:

- PascalCase for classes (including metaclasses).

- Lowercased snake_case for functions, methods and attributes.

- Uppercased snake_case for costants

- `_` prefix for hidden/protected attributes.

- `__` prefix for private attributes.

### Respect the Field philosophy

The whole project is based on the concept of field, described in the API glossary, and any abuse of the Field class that violates its philosophy is forbidden.

The main goal of Prettyetc is to provide a language-indipendent view and APIs for configuration files, so any language dependent code, that is not in the specific parser, or serializer, implementation, is forbidden.

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

- Using welcoming and inclusive language
- Being respectful of differing viewpoints and experiences
- Gracefully accepting constructive criticism
- Focusing on what is best for the community
- Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

- The use of sexualized language or imagery and unwelcome sexual attention or advances
- Trolling, insulting/derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others' private information, such as a physical or electronic address, without explicit permission
- Other conduct which could reasonably be considered inappropriate in a professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project team by the discord server. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The project team is
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from this [gist](https://gist.github.com/PurpleBooth/b24679402957c63ec426), that is an adapted version of [Contributor Covenant](http://contributor-covenant.org)
