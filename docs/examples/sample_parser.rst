--------------------------------------------------------------------------------
Source code of an example of parser plugin (stored in examples/sample_parser.py)
--------------------------------------------------------------------------------

.. _example_sample_parser:

.. literalinclude:: ../../examples/sample_parser.py
