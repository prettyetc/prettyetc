----------------------------------------------------------------------------------------
Source code of an example of serializer plugin (stored in examples/sample_serializer.py)
----------------------------------------------------------------------------------------

.. _example_sample_serializer:

.. literalinclude:: ../../examples/sample_serializer.py
