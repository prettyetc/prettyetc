================================
Configuration languages glossary
================================

.. glossary::
    conf
    config
    configuration
     An ASCII (or Unicode if supported) text string or
     text stream that contain something convertible into fields.
     Do not confuse configuration with settings.

    lang
    language
     When we talk about configuration language, we referring to a standardized
     way to represents data, using something reproducible as a collection of fields
     (or just 1 field, of course).

     Configuration languages example are JSON, INI, XML, etc, and others.
     Databases also can be uses as a language, if they store data as fields.

    etc
     The etc configuration language is not a real standardized language,
     but it is a collection of microlanguages, all of them are structured
     similar to INI (without sections).

     The origin of this name is the \*nix /etc folder that contain the system
     (or defaults) configurations for lots of programs and also for kernel settings.

     Except for INI or JSON files, the others has an undefined simple syntax
     made by spaces, the ":" character or the "=" character (somewhere)
     and the "#" character as comment start, that remembers the sh/bash syntax.
     Examples of these files are :code:`/etc/resolv.conf /etc/sysctl.conf /etc/modules`

     Our parser aims to handle most of it, representing it using fields.

     This language is a convenient definition made by us and can be discussed deeply.
