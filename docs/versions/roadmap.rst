=================
Prettyetc roadmap
=================

.. a toctree here

General roadmap
================

First release of prettyetc is out, but we have lots of proposals for the future.
This page summaries the main points of our roadmap.

Before your reading, you will understand how we classified roadmap.
Each point is described by a pair of number from 1 to 5 and a description,
represented as :code:`work-priority: description`:

  The first number is an estimation of work.

  - 1 is a very simple task or is deploy related task.
    Not requiring a big knowledge of the components.
  - 2 is an enchant or an optimization of functions,
    improve documentation or the UI/command-line interface.
  - 3 is the standard estimation,
    may require new classes/modules,
    not creating an important subset of API.
  - 4 is adding a new class/module and provide
    a new subset of API and connect other components.
    It require a big knowledge of almost add the components.
  - 5 is rewriting some components or do a reverse-engineering of something.

  The second number is the priority.

  - 1 is a little enchant, feature or tweak, nothing important.
  - 2 is a requested feature, or a built-in plugin.
  - 3 is the main priority.
  - 4 is a very requested feature
  - (to use only in urgent situations) 5 are bugs,
    performance issues, security holes.



Core library and plugins (etccore)
----------------------------------


For the core of this project we have big plans.

- 3-3: Add utility methods to RootField.
- 5-3: Add language sniffers for helping FileMatcher to get the language.

- 3-2: add yaml support (with comments if possible).
- 1-2: try to find problems in the etc parser.
- 4-2: handle databases (pandas can be a good library for that).
- 4-2: add csv support (with a database like structure if possible).
- 5-2: handle something else such as source codes.

**Persistent tasks**

All of these task are endless, so no priorities are assigned.

- 2: Add/improve logging in all components.
- 2: Improve/fix documentation.
- 4: Add/improve tests.

Base UI interface, command-line and settings (baseui):
------------------------------------------------------

baseui doesn't require so much efforts because is an helper library,
however some enchant to command-line interface and settings manager can be done.
This interface can give also a simple cmd interface (not curses, of course).

- 3-3: Make a simple cmd ui to display the field tree.
- 3-3: Cmd ui (require the basic cmd ui above):
  display the field tree in different ways such as tree,
  tables and other.

- 4-3: create helper methods to manage and do utility functions
  (ex. find) to be exposed to UIs.
  Probably require some work in the core library.

- 5-3: Make settings available before instancing
  the main UI class (initializing level).

- 1-1: in baseui.main.UiLauncher.create_ui() use better exit codes than 1
  if any critical error occurred.

**Persistent tasks**

All of these task are endless, so no priorities are assigned.

- 2: Add/improve logging in all components.
- 2: Improve/fix documentation.
- 2: Improve command-line interface with better helps, descriptions and usages.


The Qt-based interface (qtui):
------------------------------

It's the first, official interface of the prettyetc project.
It has an huge choose of integrated themes.


I think that the setting dialog is done better (at quality level) than the main window.
It requires an huge restyling, with new designs and new features,
including shortcuts.
Restyling, however, should produce at least 2 themes, a white theme and a black one.


- 1-3: Create a custom about dialog.
- 2-3: Create a custom error dialog, that use all of BadInput attributes.
- 4-3: Add tools to view and filter data.

- 3-2: Add localization support.

- 2-2: create an enchant of qt native theme.



**Persistent tasks**

All of these task are endless, so no priorities are assigned.

- 1: add shortcuts.


Other things
------------

At last we have some miscellaneous things to do,
these thing are not sticky related to the project features itself,
but are related to image, social, spreading this project and so on.


- 1-3: create a showcase,
  including some screenshots of the ui,
  and the command-line arguments.

- 2-3: create detailed tutorials to create plugins and how to deploy them.

- 1-1: create a tutorial for using the ui.
- 1-1: create detailed explanation for installation.

- 1-1: create an issue template.
- 1-1: create the CONTRIBUTING file and a page in docs for contributing.
- 3-1: create a website, gitlab pages can be used.


Done tasks
==========


This is a list of done task, sorted by descending date.
Task in this list are removed from roadmap.


Etccore
-------

**0.2.x**

- Add xml support (with comments if possible).
- Add field attribute support (with all related documentation).
- Convert all exceptions into BadInput,
  improve DictParser.parse_file exception handling.
- Add tutorial for developing parsers plugins.

**0.3.x**

- Add utility method :meth:`.RootField.find_by_attr`.

**0.4.x**

- Add serializing support (opposed as parser support).
- Add file conversion support (require writing support)

BaseUI
------

Nothing at the moment :)



Qt UI
-----

- Added 9 different themes.


Other things
------------

- create a logo.
- create a discord server.
