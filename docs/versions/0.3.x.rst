======================
0.3.x series changelog
======================

This page contains a summary of the changelog for the 0.3.x version series
and the detailed version.


Summary
-------


**UI**

The UI has received some big changes in its internal structure,
standardizing and modularizing it.

A new details view for showing field description and attributes.

Settings view has less pages and more organized with some new configs.

The UI now support themes and 8 are built-in.

.. tip::
    If you are an end user that use only the ui,
    the sections below might not interest you.


**CORE**

Little improvements are made in this release.

RootField now supports configuration source code, before it's very difficult
to get the code outside ConfigStream or ConfigFile.

The etc parser was rewritten using the `lark-parser <https://github.com/lark-parser/lark>`_ library
the old parser is still available if the lark-parser library is not.

Other small changes are done, see the complete changelog below for more information.

**BASE UI**

Very big changes are made in this package (some can be breaking).

A new package for ui helper components is available.
The components in the package follow (when necessary), a common lifecycle pattern
that have 3 phases: init, show and close.

The old classes BaseMainUi and BaseSettingsUi was enchanted, moved and renamed in the ui package.
The BaseMainUi class is still available in the baseui.main module.

The settings manager now support object caching.

See the documentation of all components for more information.

0.3.1
-----

**UI**

Fixes:

- missing qss themes

**BaseUi**

- missing baseui.ui package


0.3.0
-----

**UI**

Fixes:

- an unwanted window shows when the first file selected from open is opened

**Etccore**

Fixes:

- xml parser fails if element text is None


0.3.0a1
-------

**UI**

Adds:

- Prettyetc logo to the window (with resource).
- File view (where configuration files are displayed) now can have different views.
- Source view to file views.
- Default view key in settings.

Fixes:

- Useless "Restore default" button.
- Cancel button restores the saved settings.
- remove baseui subpackages in qtui wheel.

Changes:

- View of a single field now has its dedicated widget.
- All Ui views has received a big rewrite,
  using the new set of ui components from :class:`~prettyetc.baseui.ui` package.

- The toolbar becomes hidden in Windows.


**Etccore**

Adds:

- source attribute to :class:`~prettyetc.etccore.langlib.root.RootField` class.

- (WIP) etc language parser using the lark-parser library.

- 2 new Exception handler for INI parser.

Fixes:

- Unexpected error if the file given to
  :class:`~prettyetc.etccore.confmgr.ConfigFile` is not readable.

- Unhandled error in :meth:`~prettyetc.etccore.confmgr.ConfigFile.automatch` method
  if the filename parser exists, but it can't parse the file.


Removes:

- clone method of :class:`~prettyetc.etccore.plugins.PluginBase` class as it isn't used.


**BaseUi**


Adds:

- :mod:`~prettyetc.baseui.ui` package, containing all the ui bases.
- :class:`~prettyetc.baseui.ui.common.CommonComponent` class, that have a lifecycle structure.
- :mod:`~prettyetc.baseui.ui.field` module,
  containing some helper classes to develop and manage quickly the field tree.

- :meth:`~prettyetc.baseui.ui.main.BaseMain.read_file` callable property to read the file.
- :class:`~prettyetc.baseui.settings.SettingsManager` class now cache its object (if the stream has name).
- :meth:`prettyetc.baseui.settings.SettingsManager.factory` class method
  to create SettingsManager object from the given input (input processing depends to input type).

- :meth:`~prettyetc.baseui.settings.SettingsManager.init_default`
  method to set default configuration keys.

- :meth:`~prettyetc.baseui.settings.SettingsManager.reset`
  method for reseting settings.


- [] operator support for :class:`~prettyetc.baseui.settings.SettingsManager` class.

- :meth:`~prettyetc.baseui.ui.settings.BaseSettings.reset` method for reseting settings.

- :func:`~prettyetc.baseui.ui.utils.read_autoselect` function to preserve
  :meth:`prettyetc.baseui.ui.main.BaseMain.open_config_file` in-place behaviour.


Fixes:

- :class:`prettyetc.baseui.main.UiLauncher` class use the full lifecycle of
  :class:`prettyetc.baseui.ui.main.BaseMain`.


Changes:

- :class:`prettyetc.baseui.BaseMainUi` class was renamed and moved to
  :class:`prettyetc.baseui.ui.main.BaseMain`.

- :class:`prettyetc.baseui.main.UiLauncher` class use
  :meth:`prettyetc.baseui.ui.main.BaseMain.read_file`
  for reading file from given paths by command-line.

- :class:`prettyetc.baseui.settings.BaseSettingsUi` class was renamed and moved to
  :class:`prettyetc.baseui.ui.settings.BaseSettings`.

- :class:`prettyetc.baseui.settings.SettingsManager` class now use the homebase library.

- :class:`prettyetc.etccore.confmgr.ConfigStream` class now raise a BadInput
  if the given stream is invalid.

Removes:

- :class:`prettyetc.baseui.ui.settings.BaseSettings` class no longer use the homebase library.


Deprecations:

- :meth:`prettyetc.baseui.ui.main.BaseMain.open_config_file` method,
  use :meth:`prettyetc.baseui.ui.main.BaseMain.read_file` instead.

- :meth:`~prettyetc.baseui.ui.settings.BaseSettings.init_config`,
  use :meth:`~prettyetc.baseui.settings.SettingsManager.init_default` instead in init_ui.


0.3.0a2
-------

**UI**

Adds:

- field details view.
- stylesheet and palette themes.

Fixes:

- BaseFieldItem.description returns field name instead of field description.


Changes:

- settings names and structures.

Removes:

- unused settings in both settings and view-



**Etccore**

Adds:

- :meth:`~prettyetc.etccore.langlib.root.RootField.find_by_attr`
  method for searching fields in tree.

Changes:

- make all members of each package and subpackage available in that.
- all :class:`~prettyetc.etccore.langlib.field.Field` subclasses are moved in
  :mod:`~prettyetc.etccore.langlib.field` module.
  All members are still attrvaiable in the :mod:`prettyetc.etccore.langlib` module.

Fixes:

- issue #1, a bug that prevents folder with glob character to be accepted.

Deprecations:

- :attr:`~prettyetc.etccore.langlib.root.RootField.metadata` attribute.
  The `__init__` metadata parameter was deprecated in 0.2.0,
  so you shouldn't have used it before.


**BaseUi**

Adds:

- Allow data update on :meth:`prettyetc.baseui.settings.SettingsManager.load` (disabled by default).

Fixes:

- Cached :class:`prettyetc.baseui.settings.SettingsManager` overridden no longer have data overridden.

Changes:

- Make all members of each package and subpackage available in that.


0.3.0a3
-------

**UI**

Adds:

- File path to status bar
- (WIP) Add a find form to the toolbar

Changes:

- Use snake_case naming style for all widgets

**Etccore**

Adds:

- :meth:`~prettyetc.etccore.langlib.root.RootField.find_by_attr`
  now have a check_all parameter that allow to check all given attributes
  or just 1 of those.


0.3.0rc1
--------

**UI**

Adds:

- UI highlighting for find

Fixes:

- Find bar visibility on windows
- Searching when type != all
