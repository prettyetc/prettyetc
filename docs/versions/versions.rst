All prettyetc versions
======================

All documentation versions and repository branch are listed here.

master branch
    - sources: `<https://gitlab.com/prettyetc/prettyetc>`_
    - docs: `<https://prettyetc.gitlab.io/prettyetc>`_

0.3.x
    - sources: `<https://gitlab.com/prettyetc/prettyetc/tree/0.3.x>`_
    - docs: `<https://prettyetc.gitlab.io/prettyetc/0.3.x>`_

0.2.x
    - sources: `<https://gitlab.com/prettyetc/prettyetc/tree/0.2.x>`_
    - docs: `<https://prettyetc.gitlab.io/prettyetc/0.2.x>`_

0.1.x
    - sources: `<https://gitlab.com/prettyetc/prettyetc/tree/0.1.x>`_
    - docs: `<https://prettyetc.gitlab.io/prettyetc/0.1.x>`_
