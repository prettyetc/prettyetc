======================
0.4.x series changelog
======================

This page contains a summary of the changelog for the 0.4.x version series
and a detailed one for each version.


Summary
-------

**UI**

File saving is finally supported in the UI.
That's a big goal for the whole prettyetc project!

An huge UI rework, to support editing, adding and removing fields,
then saving, using the same configuration language or another.

All the field properties can be edited by double-clicking a field value,
by using the details view in the right or by the context menu.
The details view (placed in the right) allows to edit the attributes, too.

Other improvements are in performance, especially in loading big files.

.. warning::
    PySide2 no longer supports Python 3.4, so
    we decided to drop that support, too.

.. tip::
    If you are an end user that use only the UI,
    the sections below might not interest you.



**Serializing support**

Serializing support now is ready to use!

The whole prettyetc library, including the baseui package,
was ready for the serializing and writing to file.

The module :mod:`.serializers` provides a similar API of :mod:`.parsers`
for creating language serializers similar to the parser.
In addition, serializers supports some settings.

All the languages (except for the legacy EtcParser) have them serializers.

**New languages**

Thanks to @cattai_lorenzo, now we have a Yaml language support.

Huge changes was done for our etc language.
In fact, now the language is split into 2 sublanguages, called etc-sep and etc-space.

The new sublanguages handle different cases  by specializing.
In fact, the etc-sep language is aimed in INI-style (without sections)
but the etc-space language is aimed to space separated fields, that an INI can't handle.

Also, these languages supports descriptions and readonly (commented) fields.

**CORE**

Apart from the serializing, core does not receive huge features,
but, in this release series, we decided to make to standardizations and sorting
in order to make :class:`.Field` the main structure of the project.

:class:`.Field` and :class:`.IndexableField` class now have some utilities
and new APIs for manipulating field data.

To help in debugging, the :meth:`.Field.prettify` methods
print the field in a multi-line view.

:mod:`.parsers` module was cleaned,
with a consistent number of deprecations and new classes.

**BASEUI**

Apart from the serializing, baseui does not receive huge features.

Some automations and optimizations are done in the whole package.

The most notable feature is that :class:`.BaseMain` class receives some features,
in particular the read system now can be customized using 4 different ways:

- by overriding the :py:obj:`.BaseMain.read_file` (this disables the ways below).
- by setting :py:obj:`.BaseMain.read_file` property.
- by adding a listener callback to :py:obj:`.BaseMain.read_listener`.
- by calling the :meth:`.BaseMain.bind_read` method with a callback.

**DOCS**

Huge fixes and updates are done in a not really maintained documentation.

A consistent part of the API has a one-line docstring,
missing any explanation of the API behaviours; expecially
if the entry is callable, parameters, returns and exception raised,
if any, are not explained properly.
Some docstrings are left incomplete, too.

Lots of updating, fixing and sorting page are done until now,
but that is far away to be a complete documentation.

0.4.0rc3
********

**UI**

Adds:

- A new dark theme (by @gg312).

**Etccore**

Adds:

- :meth:`.BaseSerializer.normalize_field`
  for language serializer that use python primitives.

- root description on etc-sep\*.

Changes:

- Rename prettyetc-core package to prettyetc.

Fixes:

- Fix an issue on :class:`.LoggerCreator` when using a stream as logfile.
- Fix an issue that skip :class:`.LoggerCreator` initializing.
- Fix an issue that prevents plugin modules to load if any error occurred.
- Fix :meth:`.RootField.iteritem` index.
- Fix XML serializer that adds an unwanted root node (by @Chris1101x).
- Complete INI serializer.
- Fix etc parsers that parse non-etc files and return an invalid result.

Removes:

- index_as_key :class:`.BaseSerializer` setting.


0.4.0rc2
********

**UI**

Adds:

- A better about dialog.

Fixes:

- Fix a severe bug that prevent UI to load.


**BaseUI**

Fixes:

- Fix a severe bug that removes the :mod:`.ui` package in built wheel.

**Etccore**

Fixes:

- Fix unexpected positional argument in :meth:`.ConfigFile.automatch`.


0.4.0rc1
********

**UI**

Changes:

- About screen

**Etccore**

Adds:

- PyPy beta support.
- logging auto initializing in :class:`~ConfigFileFactory`.
- :class:`~BaseSerializer` settings in :meth:`.ConfigStream.write` method parameters.
- :meth:`.Field.from_primitive` method, for simple fields.
- Binary (arithmetic and bitwise) operators.

0.4.0a4
*******

**UI**

Adds:

- Description as field tooltip.
- Close dialog.
- New (untitled) tab support, to create new files.
- Window title changes on tab change.
- Action icons (X11 only).


Changes:

- Use a text area instead of a text field for description.

Removes:

- The toolbar manager popup.

Fixes:

- Unmodified tab name on save.
- Unmodified source on save.
- Unexpected error on field deleting.
- qt init stuffs now are cross-plaform.


**Etccore**

Adds:

- `use_name` parameter to ;meth:`.IndexableField.to_primitives` method.
- Yaml parser and serializer.
- Xml serializer.
- Etc sublanguage: etc-sep and etc-space, with its parsers and serializers.


Changes:

- Initialize data on :meth:`.IndexableField.add` method for common types.


Fixes:

- Unexpected exception in RootField.source when name is None.
- Missing stream parameter passed to :meth:`.BaseSerializer.serialize_string` method
  in :meth:`.BaseSerializer.serialize_file`.

- :meth:`.PluginManager.search_modules`'s module searching by path.
- Raise a :exc:`.SerializeFailed` exception on :meth:`.ConfigStream.write` method
  instead of returing it.

Optimizations:

- Don't use the same parser more than one time in :meth:`.ConfigStream.try_parser` method.


0.4.0a3
*******

**UI**

Adds:

- A context menu for editing and removing the field.
- A menu voice in the context menu, if the field is a container,
  for adding a new field.

- Multiple selection support.
- Shortcuts for adding and removing fields.

- Save support.
- Save as support.

Changes:

- The details view now resizes at minimum before showing.

Removes:

- the "enable hidden actions" setting, as the save action now works.

Fixes:

- Unwanted tree view when default view is source.


**Etccore**

Adds:

- :meth:`.ConfigFileFactory.all_language_suffixes` method to get all the
  language suffixes.

- :meth:`.IndexableField.count` method to count recursively all the fields,
  including itself.

Changes:

- When :obj:`.Field.attributes` attribute is empty,
  None is saved instead to reduce memory footprint.

Fixes:

- An error when no element is found in :meth:`.RootField.find_by_attr`.

- Setting :attr:`.SerializeFailed.filename` to the exception in
  :meth:`.BaseSerializer.serialize_file` method.


**BaseUI**

Adds:

- :meth:`.IndexableFieldUI.create_child` method to convert a :class:`.Field` object
  to an instance of :class:`BaseFieldUI` or a subclass of it.

- :meth:`.RootFieldUI.add_field` and :meth:`.RootFieldUI.remove_field` as shortcut
  of :attr:`.RootFieldUI.tree`.

Changes:

- :meth:`.RootFieldUI.ui_builder` method now uses :meth:`.IndexableFieldUI.create_child`
  to create the fieldui tree.

Fixes:

- Unexpected error in :meth:`.BaseMain.write_file` method due to unespected override parameter.



0.4.0a2
*******

**UI**

Adds:

- A complete and full featured details view.
- Editing tree cells.

Changes:

- The tree cells no longer can be edited directly.
- Editing an element (in the tree and in the details view)
  now changes the corresponding field, so it can be saved.

Fixes:

- Crash if the theme file name has more than one dot (.) character.


**Etccore**

Adds:

- The :meth:`.IndexableField.to_primitives` method,
  that converter from :class:`.Field` to pythonic object.

- The :meth:`.PluginManager.search_modules` method,
  that search plugin modules by given path or by given setuptools' entry_point.

- The :meth:`.IndexableField.add` and :meth:`.IndexableField.remove` methods.

Changes:

- When writing, only in :class:`.ConfigFile` the stream will be truncated.

Optimizations:

- Use threads instead of processes when in :meth:`.RootField.find_by_attr` method
  when the data is small.

**BaseUI**

Adds:

- Blocking parameter on :meth:`.CommonComponent.run` method,
  for non-blocking show calls.

- :meth:`.BaseFieldUI.save` method for field saving.

Fixes:

- :meth:`.RootFieldUI._call_to_tree` method when parallel is False.


**Project**

Adds:

- Tests for all supported version of Python (from 3.4 to 3.8) in CI.

- "etccore" setup.py extra for optional dependencies of the etccore module.
- "baseui" setup.py extra for optional dependencies of the baseui module.
- "plugins" setup.py extra for optional languages support.
- "all" setup.py extra for all the extras (except for "test").

Changes:

- Move the Qt UI files (prettyetc-qt) from prettyetc/qtui
  to a dedicated folder named prettyetc_qt,
  with its setup.
- Remove the UML in useful_for_devs as it's not updated.

Removes:

- "Qt" and "Homebase" setup.py extras.



0.4.0a1
*******

**UI**

Adds:

- Now field tree are lazy loaded, so big configs can be loaded without hanging the UI.

Fixes:

- Unwanted second "Tree" button.

**Etccore**

Adds:

- `plugin_mgr` required parameter to :class:`.ConfigStream`.
- Implementation of :meth:`.ConfigStream.write` method.
- `configfile_class` parameter of :class:`.ConfigFileFactory`.
- :meth:`.Field.copyto` and :meth:`.Field.copyfrom` methods for transfering :class:`.Field` attributes.
- :meth:`.Field.prettify` for printing field attributes.
- :meth:`.IndexableField.from_primitives` and :meth:`.IndexableField.to_primitives`
  to convert Python primitives to :class:`.Field` objects and vice versa.

- :meth:`.IndexableField.iteritems` method to iter by field key and associated value.
- :meth:`.IndexableField.remove` method to remove given :class:`.Field` object from data.
- :class:`.ArrayField` and :class:`.DictField` specific implementations of
  :meth:`.IndexableField.iteritems` and :meth:`.IndexableField.remove` method.

- :meth:`.RootField.find_by_attr` method now can use processes to speed up itself.
  Process-based parallelism can be enabled through the `parallel` parameter.

- :class:`.parsers.ReadAllParser` class.

- :mod:`.serializers` module for serializing fields.

- INI and JSON serializers.

Changes:

- :class:`.FileMatcher` logic was moved to :class:`.ConfigStream`.
- :meth:`.ConfigStream.write` no longer raise NotImplementedError
  for missing serializing support as its implementation is done.

- Parsers no longer require to inhiterit :class:`.PluginBase`


Deprecations:

- :class:`.FileMatcher` class, use :class:`.ConfigStream` class instead.
- :meth:`.BaseParser.parse_line` method.
- `matcher` parameter of :class:`.ConfigStream` class.
- :class:`PrimitiveParser` and :class:`DictParser` classes.
  Replaced by :class:`~ReadAllParser` class and :meth:`IndexableField.from_primitives` method.


Fixes:

- Unhandled error in :meth:`.ConfigStream.read` when the stream is not readable.
  Now raises a NotImplementedError.

- :meth:`.Field.__hash__` method docs says that raises `ValueError` if data is not hashable,
  but it's TypeError.


**BaseUI**

Adds:

- :attr:`.BaseMain.uiname` attribute, that can be used instead of module `__prettyetc_ui__`
- :meth:`.BaseFieldUI.remove_field` method for removing fields.
- Lazy loading on :meth:`.RootFieldUI.ui_builder` method.
- Read file event system on :class:`.BaseMain`.
- :meth:`.BaseMain.read_file` method for serializing fields into a file.
- :func:`~prettyetc.baseui.ui.main.generate_main`
  function to generate :class:`.BaseMain` subclasses.

Changes:

- :meth:`.BaseMain.main` no longer requires to be overridden.
