# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#

import importlib
import os
import sys

import sphinx.ext.autodoc
import sphinx_rtd_theme

html_theme = "sphinx_rtd_theme"
sys.path.insert(0, os.path.abspath('..'))
print("sys.path", sys.path)

# -- Project information -----------------------------------------------------

project = 'prettyetc'
copyright = '2019, trollodel'
author = 'trollodel'

# The full version, including alpha/beta/rc tags
release = importlib.import_module("prettyetc.etccore").__version__

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinx.ext.intersphinx",
    "sphinx_rtd_theme",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_css_files = [
    'css/prettyetc_docs.css',
]
html_favicon = "logo.ico"

# custom theme configuration
html_theme = "sphinx_rtd_theme"
pygments_style = 'monokai'

# extensions configurations
todo_include_todos = True
autodoc_default_options = {
    'exclude-members': '__weakref__, __init__, __dict__',
    'member-order': 'bysource',
}
intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
}


# get autodoc rst
def add_line(self, line, source, *lineno):
    """
    From https://stackoverflow.com/questions/2668187/make-sphinx-generate-rst-class-documentation-from-pydoc

    Append one line of generated reST to the output.

    """
    autodoc_rst_stream.write(self.indent + line + "\n")
    self.directive.result.append(self.indent + line, source, *lineno)


def patch_autodoc():
    """Patch sphinx.ext.autodoc."""
    global autodoc_rst_stream
    autodoc_rst_stream = open("autodoc.rst", "w")
    sphinx.ext.autodoc.Documenter.add_line = add_line


# patch_autodoc()
