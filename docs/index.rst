.. prettyetc documentation master file, created by
   sphinx-quickstart on Sun Aug 11 12:22:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Prettyetc's documentation!
=====================================

.. include:: ../README.rst


.. toctree::
    :caption: Tutorials and HOW TOs
    :maxdepth: 3

    developing/userguide/index
    developing/extending/index


.. toctree::
  :maxdepth: 3
  :caption: API Reference

  prettyetc/modules


.. toctree::
  :maxdepth: 2
  :caption: Prettyetc versions

  .. versions/roadmap
  versions/versions
  versions/0.4.x
  versions/0.3.x
  versions/0.2.x


.. toctree::
  :caption: Glossaries
  :maxdepth: 2

  glossary/api
  glossary/lang
