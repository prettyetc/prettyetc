Prettyetc packaging
===================

*********************
prettyetc extras
*********************

Here is a list of prettyetc extras

- etccore
    This extra unlocks some features in the :mod:`~prettyetc.etccore` module.

    Here are listed:

    - plugin searching by setuptools' entry_points

- baseui
    This extra unlocks some features in the :mod:`~prettyetc.baseui` module.

    Here are listed:

    - platform-dependent user settings
      location using `homebase <https://homebase.readthedocs.io/en/latest/>`_

- plugins
    This extra unlocks some language plugins.

    Here are listed:

    - etc
    - Yaml

- all
    This extra install all the extras listed above

- test
    This extra is necessary to execute the tests.
