Quick start
===========

This page aims to introduce you about the Prettyetc APIs, and its architecture.

Parse and serialize a file
**************************

The :class:`.ConfigFileFactory` object allow you to create :class:`.ConfigFile`
objects, properly initialized. :class:`.ConfigFile` manages the I/O of the file.

In particular,
you can read the file through the :meth:`~.ConfigFile.read` method, and
you can write the file through the :meth:`~.ConfigFile.write` method,
:meth:`~.ConfigFile.write` allows you to specify the output language and
the :class:`~.ConfigFile` creates file for you if doesn't exists.

The read data (and the data to be written) is contained in a tree of :class:`.Field` objects.

Here is a commented example of :class:`.ConfigFile` I/O

.. code-block:: python

    from prettyetc.etccore import ConfigFileFactory

    # create the factory
    factory = ConfigFileFactory(enable_logger=True)

    # create the file instance, this can be used to read and write the real file
    file = factory.create_file("colors.json")

    # read the file and save it in a RootField
    root = file.read()

    # ...
    # do some editing
    # ...

    # write the RootField to file
    file.write(root, language="json")


Edit the field tree
*******************

After reading the file and getting the :class:`.RootField` object,
the next question probably is: "And now? How can I edit it?"

Luckily, editing the :class:`.RootField` object is really simple.
In fact, the parsed file is represented in a tree where each leaf is a :class:`.Field` object,
or a :class:`.IndexableField` object if the leaf has children.
Both :class:`.Field` and :class:`.IndexableField` use lots of Python
special methods to get a common and simpler interface. For example, :class:`.IndexableField`
allows to get a :class:`.Field` by index through the sequence protocol
(:code:`field[key]`, to clarify) in the same way how lists and dictionaries work.
Iteration works too.
For specific data types, such as lists, a set of common functions are provided.
An example is the :class:`list`'s :meth:`~.list.append` method,
available also in the :class:`.Field` counterpart :class:`.ArrayField`,
with the same signature and meaning of the original method.

"What if the method that I want is not available direcly?"
No problem, because the original data is available through the :attr:`~.Field.data` attribute.
This attribute (except for the special case when the field is readonly)
also allows to change the field data.

Creating a new field is a bit more difficult.
First, you need to pick a :class:`~Field` subclass that will handle the desired data type,
after that you need to choose a name for it (this is required, if you don't need a name set it to None),
finally you can create the field.

:class:`.Field` can accept other parameters (description, attributes, readonly),
but these are not discussed in this page.
You can find their meanings in the :ref:`API glossary`.

Let's see some :class:`~Field` class instancing examples.

.. code-block:: python

    # a string field
    text = StringField("sample", data="some text")

    # also, this is possible
    text = StringField("sample")
    text.data = "some text"

    # an integer field
    number = IntField("foo", data=123)

    # a float field
    number = FloatField("foo", data=123.45)

    # an array field
    array = ArrayField("bar",
        data=[
            StringField("a string", data="a")]
        )

    # a dictionary field with a child boolean field
    dictionary = DictField("baz",
        data={
            "x": BoolField("x", data=True),
        })


You are probably wondering: "This is too long to write and it is not so pythonic".
Let us help you (because we are lazy too) save time and keystrokes with the
:class:`~IndexableField`'s :meth:`~IndexableField.from_primitives`
that converts a python primitive type (or a collection of them) to fields.

Here is an example with manual field instancing and equivalent from_primitives:

.. code-block:: python

    # data structure
    data = {
        "color": "aqua",
        "code": {
            "rgba": [0, 255, 255, 1],
            "hex": "#0FF"
        },
        "web_safe": True,
    }

    # manual field instancing

    field = DictField(
        None, {
            "color":
            StringField("color", "aqua"),
            "code":
            DictField(
                "code", {
                    "rgba":
                    ArrayField("rgba", [
                        IntField(None, 0),
                        IntField(None, 255),
                        IntField(None, 255),
                        IntField(None, 1),
                    ]),
                    "hex":
                    StringField("hex", "#0FF"),
                }),
            "web_safe":
            BoolField("web_safe", True)
        })

    # instancing with from_primitives (DictField is a subclass of IndexableField)
    field = DictField.from_primitives(data)




Summing up: a complete example of Prettyetc workflow
****************************************************

We have explained before all the core features of prettyetc,
It's now time for a complete use-case of Prettyetc.

colors.json (inspired by `Example Colors JSON File <https://www.sitepoint.com/colors-json-example/>`_)

.. code-block:: json

    {
      "colors": [
        {
          "color": "black",
          "code": {
            "rgba": [255,255,255,1],
            "hex": "#000"
          },
          "web_safe": true
        },
        {
          "color": "white",
          "code": {
            "rgba": [0,0,0,1],
            "hex": "#FFF"
          },
          "web_safe": true
        },
        {
          "color": "red",
          "code": {
            "rgba": [255,0,0,1],
            "hex": "#FF0"
          },
          "web_safe": true
        },
        {
          "color": "blue",
          "code": {
            "rgba": [0,0,255,1],
            "hex": "#00F"
          },
          "web_safe": true
        },
        {
          "color": "yellow",
          "code": {
            "rgba": [255,255,0,1],
            "hex": "#FF0"
          },
          "web_safe": true
        },
        {
          "color": "green",
          "code": {
            "rgba": [0,255,0,1],
            "hex": "#0F0"
          },
          "web_safe": true
        },
      ]
    }


Sample code

.. code-block:: python

    from prettyetc.etccore import ConfigFileFactory, DictField

    # create the factory
    factory = ConfigFileFactory(enable_logger=True)

    # create the file, this can be used to read and write the file
    file = factory.create_file("colors.json")

    # read the file and save it in a RootField
    root = file.read()

    # add a new color
    color = {
        "color": "aqua",
        "code": {
            "rgba": [0, 255, 255, 1],
            "hex": "#0FF"
        },
        "web_safe": True,
    }
    root["colors"].add(DictField.from_primitives(color))

    # change red color name
    red_color = root["colors"][2]
    red_color["color"].data = "light-red"

    # change red color codes
    red_color["code"]["rgba"][0].data = 204
    red_color["code"]["hex"].data = "#c00"

    # change red color web_safe field
    red_color["web_safe"].data = False

    # write the RootField to file (beautified json)
    file.write(root, language="json", beautify=True)
