Installation
============


The prettyetc project requires at least Python 3.4

.. note::
    If you have both Python 2 and 3 installed in your system,
    you should use python3 instead of python and
    pip3 instead of pip.


Quick install (pip)
-------------------

The prettyetc APIs package is available on PyPI as "prettyetc"

.. code-block:: bash

    pip install prettyetc

The basic installation offers only some feature of prettyetc,
if you want all of them, you should use the `all` extra as follows.

.. code-block:: bash

    pip install prettyetc[all]


Installation from source
------------------------

We recommend the installation from pip, if it is not possible,
you can install it manually or use it directly from the project root.


If you want to install it from source, you will need to run the `setup.py` script
(requires setuptools) as follows.

.. code-block:: bash

    python setup.py install

.. seealso::
    :ref:`prettyetc extras` for a detailed reference of extras.
