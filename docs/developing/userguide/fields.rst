Using the :class:`.Field` class
===============================

:class:`.Field` hierarchy
*************************

The :class:`.Field` class is rarely used directly. Its subclasses are used instead.

Here is a list of Prettyetc's :class:`.Field` hierarchy,
with the fundamental classes marked with a "\*" and
the typed fields (that has :class:`.TypedField` as metaclass)
marked with a "!"

.. code-block::

    Field *
     +-- NameField
     +-- SeparatedField
     |    +-- StringSeparatedField !
     +-- StringField !
     +-- IntField !
     +-- BoolField !
     +-- FloatField !
     |
     +-- IndexableField *
          +-- ArrayField !
          +-- DictField !
          |
          +-- NestedField
               +-- RootField *
                    +-- TableRootField
