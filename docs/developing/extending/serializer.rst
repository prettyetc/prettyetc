===========================================
How to develop a language serializer plugin
===========================================

Here's a detailed step-by-step guide to create a serializer plugin file.

1. createing a Python file like this:

.. code::

    from prettyetc.etccore.langlib import BaseSerializer

    __all__ = ("LangSerializer", )

    class LangSerializer(BaseSerializer):
        # some metadata
        LANGUAGES = ("lang", "lang2")

        # some methods
        def parse_string(self, string):
            # some code

.. In this snippet we use:class:`.BaseSerializer`,
.. but you can use also provided subclasses of it.

2. Adding the metadata information:

- **loggername**
  Set the logger name in logging syntax
  (if you want to use the integrated logger).

- **LANGUAGES**
   A tuple of languages that the parser provides.
   Languages must be lowercase.

- **STANDARD_EXTENSION**
   A string containing the standard file extension associated to the language.


3. Implementing the abstract methods

In contrast to parsers, serializers does not have differents ways to do the operation,
so the problem is delegated to the serializer itself.
However, the library offers some settings and 2 differents outputs for the serialized field tree.

The possibities are described in the list below:


- :meth:`.BaseSerializer.serialize_string`
  Serialize a field (usually the field must be an :class:`.IndexableField` object),
  and return the result as a string.

  There are some cases where a serialization by a stream can be more efficent than a string,
  so the method has an optional parameter, called stream, that can contain a writable stream
  and this can be used instead of the string.

- :attr:`.BaseSerializer.settings`
  Containing a :class:`~collection.nametuple` with some settings to change the
  output style, or enforcing/suppressing some rules in serialization.

4. Processing the field tree

The serialized tree should be a :class:`str`, no bytes-like or array-like object are allowed.


.. tip::
    While walking in the field tree, We suggest to use the :func:`isinstance` function and passing,
    the less specialized field object.

.. seealso::
    We highly suggest to see the complete example of a parser plugin can be found in the git repo,
    in the file :doc:`examples/sample_parser.py <../../examples/sample_parser>`

    .. versionadded:: 0.2.0
