The etccore.logger module
=========================

.. if there are subpackages of this package
..
.. Subpackages
.. -----------
..
 .. toctree::
..
..    subpackages
..
..
.. Submodules
.. ----------
..
 .. toctree::
..
..     submodules
..
.. Module content
.. --------------




.. automodule:: prettyetc.etccore.logger
   :members:
   :show-inheritance:
