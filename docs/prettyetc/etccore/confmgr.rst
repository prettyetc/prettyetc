The etccore.confmgr module
==========================

.. if there are subpackages of this package
..
.. Subpackages
.. -----------
..
 .. toctree::
..
..    subpackages
..
..
.. Submodules
.. ----------
..
 .. toctree::
..
..     submodules
..
.. Module content
.. --------------


.. automodule:: prettyetc.etccore.confmgr
   :members:
   :show-inheritance:
