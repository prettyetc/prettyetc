The baseui.main module
=========================

.. if there are subpackages of this package
..
.. Subpackages
.. -----------
..
 .. toctree::
..
..    subpackages
..
..
.. Submodules
.. ----------
..
 .. toctree::
..
..     submodules
..
.. Module content
.. --------------

.. automodule:: prettyetc.baseui.main
   :members:
   :show-inheritance:
