The baseui package
==================

Subpackages
-----------

.. toctree::
   :titlesonly:

   ui/ui



Submodules
----------

.. toctree::
    :titlesonly:

    main
    settings
    cmdargs
    utils

======
BaseUI
======

.. automodule:: prettyetc.baseui
