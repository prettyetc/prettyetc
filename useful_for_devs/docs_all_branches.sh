#!/bin/sh

# from https://gitlab.com/gitlab-org/gitlab-pages/issues/33
# and changed for sphinx

# OUT_DIR="$1"
INITIAL_BRANCH=$(git rev-parse --abbrev-ref HEAD)

# mkdir -p $OUT_DIR

pip install sphinx sphinx-rtd-theme
# It is important to start with master branch:
echo "First checkout in $INITIAL_BRANCH"
git checkout $INITIAL_BRANCH

cd docs
sphinx-build ../docs build -b html
mkdir -p ../public
mv build/* ../public
cd ..

# # Generating documentation for each other branch in a subdirectory
# for BRANCH in $(git branch --remotes --format '%(refname:lstrip=3)' | grep -Ev '^(HEAD|master)$')
#     do
#     echo "Checkout in $BRANCH"
#     git checkout $BRANCH
#
#     cd docs
#     sphinx-build . build -b html
#     mkdir -p ../public/$BRANCH
#     mv build/* ../public/$BRANCH
#     cd ..
# done


# extra branches

# start 0.1.x
BRANCH="0.1.x"

echo "Clone branch $BRANCH"
git clone $CI_PROJECT_URL -b $BRANCH $BRANCH

cd $BRANCH

# build docs
cd docs
sphinx-build . build -b html
cd ..
# move pages
mkdir -p ../public/$BRANCH
mv docs/build/* ../public/$BRANCH

cd ..
# end 0.1.x



# start 0.2.x
BRANCH="0.2.x"

echo "Clone branch $BRANCH"
git clone $CI_PROJECT_URL -b $BRANCH $BRANCH

cd $BRANCH

# build docs
cd docs
sphinx-build . build -b html
cd ..
# move pages
mkdir -p ../public/$BRANCH
mv docs/build/* ../public/$BRANCH

cd ..
# end 0.2.x

# start 0.3.x
BRANCH="0.3.x"

echo "Clone branch $BRANCH"
git clone $CI_PROJECT_URL -b $BRANCH $BRANCH

cd $BRANCH

# build docs
cd docs
sphinx-build . build -b html
cd ..
# move pages
mkdir -p ../public/$BRANCH
mv docs/build/* ../public/$BRANCH

cd ..
# end 0.3.x

git checkout $INITIAL_BRANCH
