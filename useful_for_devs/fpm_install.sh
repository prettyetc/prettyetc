#!/bin/bash
DEB_OUT="python3-prettyetc-0.2.0rc1_all.deb"

echo "PWD: $PWD"

ln -s setup_core.py setup.py
rm -f dist/$DEB_OUT

DESCRIPTION="
$(cat README.rst)"


sudo fpm \
    \
    -s python \
    -t deb \
    \
    --log info \
    -p dist/$DEB_OUT \
    \
    -n python3-prettyetc \
    -m trollodel \
    --license gpl3 \
    --category python \
    --description "$DESCRIPTION" \
    \
    --deb-priority optional \
    \
    -d "python3-all >= 3.4" \
    --provides prettyetc \
    \
    --python-bin python3 \
    --python-pip pip3 \
    \
    --python-install-bin /usr/bin \
    --python-install-lib /usr/lib/python3/dist-packages \
    --python-scripts-executable python3 \
    \
    setup.py

sudo chmod 777 dist/$DEB_OUT
