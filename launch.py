#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Test the ui launcher."""

from prettyetc_qt.prettyetc_qt import __main_class__ as MainUI

if __name__ == '__main__':
    MainUI.main()
