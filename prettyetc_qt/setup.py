#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Setup the prettyetc Qt UI."""
import importlib
import sys
from os import path

from setuptools import find_packages, setup

# init setup variables
here = path.abspath(path.dirname(__file__))
sys.path.insert(0, path.dirname(here))
qtui_version = importlib.import_module("prettyetc_qt").__version__

# Get the long description from the README file
with open(path.join(here, '../README.rst'), encoding='utf-8') as f:
    long_description = f.read()
setup(
    # package information
    name='prettyetc-qt',
    version=qtui_version,
    author='trollodel',

    # descriptions
    description=
    'See your configuration files using a pretty and universal interface.',
    long_description=long_description,
    long_description_content_type='text/x-rst',

    # links to project
    url='https://gitlab.com/prettyetc/prettyetc',

    # For a list of valid classifiers, see https://pypi.org/classifiers/
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 5 - Production/Stable',

        # Indicate who your project is intended for
        # 'Intended Audience :: Developers',
        # 'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',

        # Python supported versions
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3',
        # 'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',

        # System requirements
        'Operating System :: OS Independent',

        # Topics
        'Topic :: Documentation :: Sphinx',
        'Topic :: System :: Systems Administration',
        'Topic :: Text Processing',
        'Topic :: Utilities',

        # Audience
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
    ],
    keywords='config json ini etc xml qt',

    # packages setup
    packages=find_packages(here, exclude=["prettyetc_qt.components.design"]),
    package_dir={"prettyetc_qt": "prettyetc_qt/prettyetc_qt"},

    # If there are data files included in your packages that need to be
    # installed, specify them here.
    package_data={
        '': ['*.qss', 'GTRONICK-QSS/*'],
    },
    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    #
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    # data_files=[('my_data', ['data/data_file'])],

    ### Requirements
    python_requires='>=3.5, <4',

    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=['PySide2', "prettyetc[all] >= 0.4.0rc2"],

    # List additional groups of dependencies here (e.g. development
    # dependencies). Users will be able to install these using the "extras"
    # syntax, for example:
    #
    #   $ pip install sampleproject[dev]
    #
    # Similar to `install_requires` above, these must be valid existing
    # projects.
    extras_require={
        # 'test': ['pytest'],
        # 'samples': []
    },

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    entry_points={
        'console_scripts': [
            'prettyetc-qt=prettyetc_qt:uilaunch',
        ],
    },

    # List additional URLs that are relevant to your project as a dict.
    #
    # This field corresponds to the "Project-URL" metadata fields:
    # https://packaging.python.org/specifications/core-metadata/#project-url-multiple-use
    project_urls={
        'Bug Reports': 'https://gitlab.com/prettyetc/prettyetc/issues',
        #     'Funding': 'https://donate.pypi.org',
        #     'Say Thanks!': 'http://saythanks.io/to/example',
        "Documentation": "https://prettyetc.gitlab.io/prettyetc/",
        'Source': 'https://gitlab.com/prettyetc/prettyetc',
    },
)
