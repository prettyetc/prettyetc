# Resource object code (Python 3)
# Created by: object code
# Created by: The Resource Compiler for Qt version 5.14.2
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore

qt_resource_data = b"\
\x00\x00\x02(\
[\
Paths]\x0aPrefix=/u\
sr\x0aArchData=lib/\
x86_64-linux-gnu\
/qt5\x0aBinaries=li\
b/qt5/bin\x0aData=s\
hare/qt5\x0aDocumen\
tation=share/qt5\
/doc\x0aExamples=li\
b/x86_64-linux-g\
nu/qt5/examples\x0a\
Headers=include/\
x86_64-linux-gnu\
/qt5\x0aHostBinarie\
s=lib/qt5/bin\x0aHo\
stData=lib/x86_6\
4-linux-gnu/qt5\x0a\
HostLibraries=li\
b/x86_64-linux-g\
nu\x0aImports=lib/x\
86_64-linux-gnu/\
qt5/imports\x0aLibr\
aries=lib/x86_64\
-linux-gnu\x0aLibra\
ryExecutables=li\
b/x86_64-linux-g\
nu/qt5/libexec\x0aP\
lugins=lib/x86_6\
4-linux-gnu/qt5/\
plugins\x0aQml2Impo\
rts=lib/x86_64-l\
inux-gnu/qt5/qml\
\x0aSettings=/etc/x\
dg\x0aTranslations=\
share/qt5/transl\
ations\x0a\
"

qt_resource_name = b"\
\x00\x02\
\x00\x00\x07\x84\
\x00q\
\x00t\
\x00\x03\
\x00\x00l\xa3\
\x00e\
\x00t\x00c\
\x00\x07\
\x08t\xa6\xa6\
\x00q\
\x00t\x00.\x00c\x00o\x00n\x00f\
"

qt_resource_struct = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x02\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x0a\x00\x02\x00\x00\x00\x01\x00\x00\x00\x03\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x16\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
\x00\x00\x01q\xefR\x08\x98\
"

def qInitResources():
    QtCore.qRegisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

def qCleanupResources():
    QtCore.qUnregisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

qInitResources()
