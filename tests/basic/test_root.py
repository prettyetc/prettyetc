#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test Root fields.
"""

# pylint: disable=E1120, W0703

import io

import pytest
from hypothesis import HealthCheck, given, settings
from hypothesis.strategies import booleans, lists, none, text

from prettyetc.etccore.langlib import Field, RootField, TableRootField

from .common import (
    DICT_PRIMITIVES, FIELD_ATTRIBUTE_SELECT, PRIMITIVES_NO_COLLECTIONS,
    BaseTest, create_field, create_field_tree, create_tree_and_samples)


class TestRootField(BaseTest):
    """Test root fields."""

    def sample_find_test(self, sample: Field, root: RootField, *attributes,
                         **find_kwargs):
        """Test single sample by given tree and attributes to check."""
        # try:
        if sample not in (root, root.data):
            # find_by_attr doesn't find attributes in self.
            attrs_param = {key: getattr(sample, key) for key in attributes}
            find_kwargs.update(attrs_param)
            found_field = root.find_by_attr(**find_kwargs)
            # assert sample == found_field
            if find_kwargs.get("check_all", True):

                for key, val in attrs_param.items():
                    assert val == getattr(found_field, key)
            else:
                for key, val in attrs_param.items():
                    if val == getattr(found_field, key):
                        break
                else:
                    assert False
        # except Exception:
        #     # debug code
        #     print("found_field", repr(root), "sample", sample)
        #     raise

    @settings(
        suppress_health_check=(HealthCheck.too_slow,
                               HealthCheck.data_too_large))
    @given(
        name=text(),
        tree=create_field_tree(),
        description=text(),
        attributes=DICT_PRIMITIVES)
    def test_init_good(self, name, tree, description: str, attributes: dict):
        """Test RootField init."""
        attributes["langname"] = ""
        root = self.create_field(
            name,
            tree,
            description,
            attributes,
            False,
            fieldtype=RootField,
        )
        assert isinstance(root, RootField)

    @pytest.mark.skip(
        "We do not decided if root field must be blocked to only lists and dicts."
    )
    @given(
        name=text(),
        tree=PRIMITIVES_NO_COLLECTIONS,
        description=text(),
        attributes=DICT_PRIMITIVES)
    def test_init_bad(self, name, tree, description: str, attributes: dict):
        """Test RootField init."""
        self.create_field(
            name,
            tree,
            description,
            attributes,
            False,
            fail=True,
            fieldtype=RootField)

    @settings(
        suppress_health_check=(HealthCheck.too_slow,
                               HealthCheck.data_too_large))
    @given(
        name=text(),
        tree=create_field_tree(),
        description=text(),
        attributes=DICT_PRIMITIVES)
    def test_pickle_integrity(self, name, tree, description, attributes):
        """Test if dumping and reload preserve the result."""
        attributes["langname"] = ""
        root = self.create_field(
            name,
            tree,
            description,
            attributes,
            False,
            checks=False,
            fieldtype=RootField)
        assert isinstance(root, RootField)
        stream = io.BytesIO()

        root.to_pickle(stream)
        stream.seek(0)

        new_root = RootField.from_pickle(stream)
        assert isinstance(new_root, RootField)
        assert root == new_root
        assert root.description == new_root.description
        assert root.attributes == new_root.attributes

    @settings(
        suppress_health_check=(
            HealthCheck.too_slow,
            HealthCheck.data_too_large,
            HealthCheck.filter_too_much,
        ),
        deadline=None,
        # I gave 40 examples for check
        max_examples=360,
    )
    @given(
        root=create_field(
            fieldtype=RootField,
            data=none(),
            readonly=False,
        ),
        tree=create_tree_and_samples(
            max_leaves=12,
            max_samples=4,
            n_samples=None,
        ),
        attrs=lists(
            FIELD_ATTRIBUTE_SELECT,
            unique=True,
            min_size=1,
        ),
        parallel=booleans(),
        check_all=booleans())
    def test_find(self, root: RootField, tree, attrs: list, parallel: bool,
                  check_all: bool):
        """
        Universal test for :meth:`~.RootField.find_by_attr` methods.

        This test case controls
        almost all the features of :meth:`~.RootField.find_by_attr`,
        by giving some sample to be found and changing the method parameters.

        In details all the checks:

        - find a sample in the tree
        - find some samples in the tree
        - find by a field attribute
        - find by n field attributes
        - find by all field attributes
        - find by n field attributes, but checking if just one is correct
        - find without parallelism
        - find with thread-based parallelism (small trees)
        - find with process-based parallelism (small trees)
        """
        # assume(isinstance(root, RootField))
        # assume(isinstance(tree, (ArrayField, DictField, list, dict)))
        tree, samples = tree
        root.data = tree
        for sample in samples:
            self.sample_find_test(
                sample, root, *attrs, parallel=parallel, check_all=check_all)


class TestTableRoot(BaseTest):
    """Test root table fields."""

    @settings(
        suppress_health_check=(HealthCheck.too_slow,
                               HealthCheck.data_too_large))
    @given(
        root=create_field(
            fieldtype=TableRootField,
            data=none(),
        ),
        col_name=text(),
        col_data=lists(create_field(type_by_data=True)),
        col_description=text(),
        col_attributes=DICT_PRIMITIVES,
    )
    def test_col_add(self, root, col_name, col_data, col_description,
                     col_attributes):
        """Test if elements are added successfully to the root field."""
        field = root.add_col(
            col_name,
            *col_data,
            description=col_description,
            attributes=col_attributes)
        assert field == root[col_name]
        for field_elm, col_elm in zip(field, root[col_name]):
            assert field_elm == col_elm
