#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test :class:`~prettyetc.etccore.plugins.PluginManager` class features.
"""
import gc
import logging
import sys
import types

import pytest
from hypothesis import HealthCheck, given, settings
from hypothesis.strategies import (
    booleans, composite, dictionaries, from_regex, integers, lists, none,
    one_of, sampled_from, tuples)
from prettyetc.etccore import LoggerCreator, PluginManager
from prettyetc.etccore.langlib import BaseParser, BaseSerializer

from .common import DEFAULT_DATA, INDENTIFIERS_ONLY

# pylint: disable=E1120, W0201, W0212


def setup_module(_):
    """Setup the logger."""
    _ = LoggerCreator(
        logging.getLogger("prettyetc"),
        logfile="test_prettyetc.log",
        log_level=logging.DEBUG,
    )
    assert LoggerCreator.ROOT_LOGGER != logging.root, "Failed to create logger"


@composite
def create_etccore_plugin(draw,
                          valid: bool = None,
                          classes: tuple = (BaseParser, BaseSerializer),
                          template: str = "{}",
                          bad_classes: tuple = (int, float, type, dict, list,
                                                object)) -> type:
    """
    Generate a plugin class that can be parsed by
    :class:`~prettyetc.etccore.plugins.PluginManager`.
    """
    if valid is None:
        valid = draw(booleans())

    valid_bases = valid_attrs = True
    if not valid:
        while (valid_bases, valid_attrs) == (True, True):
            valid_bases, valid_attrs = draw(
                lists(
                    booleans(),
                    min_size=2,
                    max_size=2,
                ))
    name = template.format(draw(INDENTIFIERS_ONLY))

    attrs = draw(dictionaries(INDENTIFIERS_ONLY, DEFAULT_DATA))

    if valid_bases:
        bases = draw(tuples(sampled_from(classes)))
    else:
        bases = (draw(sampled_from(bad_classes)),)
        attrs["_bad_bases"] = True

    if valid_attrs:
        attrs["LANGUAGES"] = draw(
            lists(
                INDENTIFIERS_ONLY,
                unique=True,
                min_size=1,
            ))

        if BaseParser in bases:
            attrs["PREFIXES"] = draw(lists(INDENTIFIERS_ONLY))
            attrs["SUFFIXES"] = draw(lists(from_regex(".?[a-zA-Z_]+")))
        elif BaseSerializer in bases:
            attrs["STANDARD_EXTENSION"] = draw(from_regex(".?[a-zA-Z_]+"))
    else:
        # if valid_bases is True,
        # PluginManager can't check the LANGUAGES attribute
        # because it is given by BaseParser or BaseSerializer.
        # However BaseParser and BaseSerializer provides an empty tuple as
        # LANGUAGES, so it cannot be added to any loaded_* dict.

        attrs["_bad_attrs"] = True

    cls = type(name, bases, attrs)
    cls.__module__ = "test.basic.test_PM.create"

    return cls


@composite
def create_etccore_plugin_module(draw,
                                 modname: str = None,
                                 valid: bool = None,
                                 use_all: bool = None,
                                 max_classes: int = 20,
                                 **plugin_kwargs) -> (types.ModuleType, list):
    """
    Generate a python module containing one or more plugin classes that can be parsed by
    :class:`~prettyetc.etccore.plugins.PluginManager`.
    """
    if modname is None:
        modname = draw(INDENTIFIERS_ONLY)

    clscount = draw(integers(min_value=1, max_value=max_classes))

    mod = types.ModuleType(modname)
    mod._valid = bool(valid)

    if valid:
        use_all = True
    elif use_all is None:
        use_all = draw(booleans())

    if valid is None and draw(booleans()):
        mod.__all__ = []
    else:
        mod.__all__ = None

    classes = {}
    for _ in range(clscount):
        if valid is None:
            _valid = draw(booleans())
        else:
            _valid = valid

        cls = draw(create_etccore_plugin(valid=_valid, **plugin_kwargs))
        name = cls.__name__
        if name not in classes:
            setattr(mod, name, cls)
            setattr(cls, "__module__", modname)
            classes[name] = cls
        if mod.__all__ is not None:
            mod.__all__.append(name)

    if valid:
        mod.__all__ = tuple(x.__name__ for x in classes.values())

    return mod, classes


class TestPluginManager(object):
    """Test plugin loading."""

    def setup_method(self, _):
        """Create plugin manager for every test case."""
        self.manager = PluginManager()
        self.modules = []
        gc.collect()

    def teardown_method(self, _):
        """Clean Plugin manager and created modules."""
        del self.manager
        modules = sys.modules.values()
        for mod in self.modules:
            if mod in modules:
                del sys.modules[mod.__name__]

        del self.modules[:]
        gc.collect()

    @settings(suppress_health_check=(HealthCheck.too_slow,))
    @given(cls=create_etccore_plugin(valid=True))
    def test_plugin_load_cls_good(self, cls: type):
        """
        Test :meth:~prettyetc.etccore.plugins.PluginManager.load_plugin`
        method with valid classes.
        """
        assert issubclass(
            cls,
            (BaseParser, BaseSerializer)), "Failed to create test requirements"
        self.manager.load_plugin(cls)

        parsers = self.manager.loaded_parsers.values()
        serializers = self.manager.loaded_serializers.values()
        if issubclass(cls, BaseParser):
            assert cls in parsers, (
                "BaseParser subclass not in PluginManager.loaded_parsers")

        elif issubclass(cls, BaseSerializer):
            assert cls in serializers, (
                "BaseSerializer subclass not in PluginManager.loaded_serializers"
            )

    @settings(suppress_health_check=(HealthCheck.too_slow,))
    @given(cls=one_of(create_etccore_plugin(valid=False), none()))
    def test_plugin_load_cls_bad(self, cls: type):
        """Test :meth:~prettyetc.etccore.plugins.PluginManager.load_plugin` method."""
        invalid_bases = getattr(cls, "_bad_bases", False)
        if invalid_bases or cls is None:
            with pytest.raises(TypeError):
                self.manager.load_plugin(cls)
                pytest.fail(
                    "Type checks are not done.\nHere is the class mro: {}".
                    format(cls.mro()))
        else:
            parsers = self.manager.loaded_parsers.values()
            serializers = self.manager.loaded_serializers.values()
            if issubclass(cls, BaseParser):
                assert not cls in parsers, (
                    "BaseParser subclass in PluginManager.loaded_parsers")

            elif issubclass(cls, BaseSerializer):
                assert not cls in serializers, (
                    "BaseParser subclass in PluginManager.loaded_parsers")

    @pytest.mark.skip(
        "Given classes can have the same languages, so they fail he test.")
    @settings(
        suppress_health_check=(HealthCheck.too_slow, HealthCheck.data_too_large,
                               HealthCheck.filter_too_much),
        max_examples=50,
    )
    @given(mod=create_etccore_plugin_module(valid=True))
    def test_plugin_fetch_mod_good(self, mod: tuple):
        """
        Test :meth:`~prettyetc.etccore.plugins.PluginManager.load_plugin`
        method with valid classes.
        """
        assert len(mod) == 2, "Failed to create test requirements"
        mod, classes = mod
        self.modules.append(mod)

        self.manager.fetch_module(mod)

        parsers = self.manager.loaded_parsers.values()
        serializers = self.manager.loaded_serializers.values()
        for name, cls in classes.items():
            assert getattr(
                mod, name,
                False), "Failed to create test requirements, invalid module"

            if issubclass(cls, BaseParser):
                assert cls in parsers, (
                    "BaseParser subclass not in PluginManager.loaded_parsers")

            elif issubclass(cls, BaseSerializer):
                assert cls in serializers, (
                    "BaseSerializer subclass not in PluginManager.loaded_serializers"
                )

    @settings(
        suppress_health_check=(HealthCheck.too_slow,
                               HealthCheck.data_too_large),
        max_examples=50,
    )
    @given(mod=create_etccore_plugin_module(valid=False))
    def test_plugin_fetch_mod_bad(self, mod: tuple):
        """
        Test :meth:~prettyetc.etccore.plugins.PluginManager.load_plugin`
        method with valid classes.
        """
        assert len(mod) == 2, "Failed to create test requirements"
        mod, classes = mod
        self.modules.append(mod)
        # assume(mod.__all__ is None or len(mod.__all__) != len(classes))

        self.manager.fetch_module(mod)

        parsers = self.manager.loaded_parsers.values()
        serializers = self.manager.loaded_serializers.values()
        for cls in classes.values():
            if issubclass(cls, BaseParser):
                assert not cls in parsers, (
                    "BaseParser subclass in PluginManager.loaded_parsers")

            elif issubclass(cls, BaseSerializer):
                assert not cls in serializers, (
                    "BaseParser subclass in PluginManager.loaded_parsers")
