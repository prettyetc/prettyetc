#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Test file parsing."""
import logging
import os
import shutil
import sys
import traceback
from tempfile import NamedTemporaryFile

import pytest
from hypothesis import given, settings
from hypothesis.strategies import integers, sampled_from

from prettyetc.etccore import (
    BadInput, ConfigFile, ConfigFileFactory, LoggerCreator)

try:
    from deepdiff import DeepDiff
except ImportError:
    pass


def setup_module(_):
    """Setup the logger."""
    _ = LoggerCreator(
        logging.getLogger("prettyetc"),
        logfile=None,
        log_level=logging.DEBUG,
        allow_print=False,
        root=True,
    )
    assert LoggerCreator.ROOT_LOGGER != logging.root, "Failed to create logger"


class TestConfigFile(object):
    """Test ConfigFile through config file factory."""
    factory = ConfigFileFactory()

    @given(sampled_from(os.listdir("testconf")))
    def test_configfile_factoring_good(self, file: str):
        """Create a config file from factory."""
        conffile = self.factory(os.path.join("testconf", file))
        assert isinstance(conffile, ConfigFile)

    @pytest.mark.skipif(
        sys.version_info < (3, 5), reason="Requires python3.5 or higher")
    @settings(deadline=1000)
    @given(
        sampled_from(os.listdir("testconf")),
        integers(min_value=2, max_value=4),
    )
    def test_configfile_integrity_io(self, file: str, io_cycles: int):
        """Test language save integrity."""

        with NamedTemporaryFile(
                mode="w+", suffix=os.path.splitext(file)[1],
                delete=False) as stream:
            shutil.copyfileobj(open(os.path.join("testconf", file)), stream)
            path = stream.name

        conffile = self.factory(path)
        roots = []
        try:
            for _ in range(io_cycles):
                root = conffile.read()
                if isinstance(root, BadInput):
                    pytest.fail("Failed to parse {}\nReason: {}".format(
                        file, "".join(
                            traceback.format_exception(
                                BadInput,
                                root,
                                root.__traceback__,
                            ))))

                roots.append(root)
                conffile.write(root, language=root.attrs["langname"])

                # restore stream status
                conffile.stream.flush()
                conffile.stream.seek(0)

            for root in roots[1:]:
                diff = DeepDiff(
                    roots[0],
                    root,
                    # ignore order because the pre-3.6 broke the comparison
                    ignore_order=sys.version_info < (3, 6),
                )
                assert not diff
        finally:
            os.remove(path)
