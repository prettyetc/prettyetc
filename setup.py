#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Prettyetc core setup file."""

import sys
from os import path

from setuptools import find_packages, setup

from prettyetc.etccore import __version__ as etccore_version

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

# create extras
extras_require = {
    'etccore': ["setuptools"],
    'baseui': ["homebase"],
    'plugins': ["lark-parser", "ruamel.yaml"],
    "test": ['pytest', "hypothesis", "deepdiff"]
}

if sys.version_info < (3, 5):
    # ruamel.yaml is not supported in py3.4
    extras_require["plugins"].remove("ruamel.yaml")

# extras_require["test"] += extras_require["plugins"]
extras_require[
    "all"] = extras_require["etccore"] + extras_require["baseui"] + extras_require["plugins"]

setup(
    name='prettyetc',
    version=etccore_version,
    description='Core library of the prettyetc project',
    long_description=long_description,
    long_description_content_type='text/x-rst',

    # This should be a valid link to your project's main homepage.
    url='https://gitlab.com/prettyetc/prettyetc',
    author='trollodel',

    # Classifiers help users find your project by categorizing it.
    # For a list of valid classifiers, see https://pypi.org/classifiers/
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 5 - Production/Stable',

        # Indicate who your project is intended for
        # 'Intended Audience :: Developers',
        # 'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',

        # Python supported versions
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3 :: Only',

        # System requirements
        'Operating System :: OS Independent',

        # Topics
        'Topic :: Text Processing',
        'Topic :: Utilities',
        'Topic :: System :: Systems Administration',
        'Topic :: Documentation :: Sphinx',

        # Audience
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
    ],
    keywords='configurations json ini etc qt',

    # You can just specify package directories manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(
        include=("prettyetc.baseui", "prettyetc.baseui.*", "prettyetc.etccore",
                 "prettyetc.etccore.*")),
    python_requires='>=3.4, <4',

    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=['typing;python_version<"3.5"'],
    extras_require=extras_require,
    tests_require=['pytest', "hypothesis"],

    # tests
    test_module="tests",

    # If there are data files included in your packages that need to be
    # installed, specify them here.
    # package_data={
    #     'sample': ['package_data.dat'],
    # },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    #
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    # data_files=[('my_data', ['data/data_file'])],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    entry_points={
        # 'console_scripts': [
        #     'prettyetc-ui=prettyetc.baseui:uilaunch',
        # ],
    },

    # List additional URLs that are relevant to your project as a dict.
    #
    # This field corresponds to the "Project-URL" metadata fields:
    # https://packaging.python.org/specifications/core-metadata/#project-url-multiple-use
    project_urls={
        'Bug Reports': 'https://gitlab.com/prettyetc/prettyetc/issues',
        #     'Funding': 'https://donate.pypi.org',
        #     'Say Thanks!': 'http://saythanks.io/to/example',
        "Documentation": "https://prettyetc.gitlab.io/prettyetc/",
        'Source': 'https://gitlab.com/prettyetc/prettyetc',
    },
)
